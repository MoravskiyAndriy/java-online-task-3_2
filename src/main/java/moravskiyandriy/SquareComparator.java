package moravskiyandriy;

import java.util.Comparator;

public class SquareComparator implements Comparator<Shape> {
    public int compare(Shape o1, Shape o2) {
        if (o1.square > o2.square) {
            return 1;
        } else if (o1.square < o2.square) {
            return -1;
        }
        return 0;
    }
}
