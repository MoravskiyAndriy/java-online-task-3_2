package moravskiyandriy;

abstract class Shape {
    protected double square;
    protected double perimeter;
    protected String color;

    abstract double getSquare();

    abstract void setSquare();

    abstract double getPerimeter();

    abstract void setPerimeter();

    abstract String getColour();

    abstract void setColour(String s);

}
