package moravskiyandriy;

import java.util.ArrayList;
import java.util.Collections;

public class Task3_2 {
    public static void main(String[] args) {
        ArrayList<Shape> list = new ArrayList<Shape>();
        Circle c1 = new Circle(3, "red");
        list.add(c1);
        Circle c2 = new Circle(3, "yellow");
        list.add(c2);
        Circle c3 = new Circle(5, "green");
        list.add(c3);
        Rectangle r1 = new Rectangle(3, 4);
        list.add(r1);
        Rectangle r2 = new Rectangle(1.5, 3.5, "blue");
        list.add(r2);
        Rectangle r3 = new Rectangle(5, 5, "red");
        list.add(r3);
        Triangle t1 = new Triangle(3, 4, 5);
        list.add(t1);
        Triangle t2 = new Triangle(1.5, 2.5, 3.5, "yellow");
        list.add(t2);
        Collections.sort(list, new SquareComparator());
        System.out.println("Sorted by square: ");
        for (Shape s : list) {
            System.out.print(s + "square= " + s.getSquare()+"\n");
        }
        Collections.sort(list, new PerimeterComparator());
        System.out.println();
        System.out.println("Sorted by perimeter: ");
        for (Shape s : list) {
            System.out.print(s + "perimeter= " + s.getPerimeter()+"\n");
        }
    }
}
