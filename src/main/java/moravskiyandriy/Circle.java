package moravskiyandriy;

public class Circle extends Shape {
    private double radius;

    @Override
    public String toString() {
        return "Circle, ";
    }

    public Circle(double d) {
        this.radius = d;
        setSquare();
        setPerimeter();
    }

    public Circle(double d, String s) {
        this.radius = d;
        setSquare();
        setPerimeter();
        setColour(s);
    }

    public double getSquare() {
        return square;
    }

    public void setSquare() {
        this.square = Math.PI * radius * radius;
    }

    public double getPerimeter() {
        return perimeter;
    }

    public void setPerimeter() {
        this.perimeter = 2 * Math.PI * radius;
    }

    public String getColour() {
        return color;
    }

    public void setColour(String s) {
        this.color = s;
    }
}
