package moravskiyandriy;

public class Rectangle extends Shape {
    private double side1;
    private double side2;

    @Override
    public String toString() {
        return "Rectangle, ";
    }

    public Rectangle(double side1, double side2) {
        this.side1 = side1;
        this.side2 = side2;
        setSquare();
        setPerimeter();
    }

    public Rectangle(double side1, double side2, String s) {
        this.side1 = side1;
        this.side2 = side2;
        setSquare();
        setPerimeter();
        setColour(s);
    }

    public double getSquare() {
        return square;
    }

    public void setSquare() {
        this.square = side1 * side2;
    }

    public double getPerimeter() {
        return perimeter;
    }

    public void setPerimeter() {
        this.perimeter = 2 * (side1 + side2);
    }

    public String getColour() {
        return color;
    }

    public void setColour(String s) {
        this.color = s;
    }
}
