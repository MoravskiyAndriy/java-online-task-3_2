package moravskiyandriy;

import java.util.Comparator;

public class PerimeterComparator implements Comparator<Shape> {
    public int compare(Shape o1, Shape o2) {
        if (o1.perimeter > o2.perimeter) {
            return 1;
        } else if (o1.perimeter < o2.perimeter) {
            return -1;
        }
        return 0;
    }
}
