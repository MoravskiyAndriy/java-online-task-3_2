package moravskiyandriy;

public class Triangle extends Shape {
    private double side1;
    private double side2;
    private double side3;

    @Override
    public String toString() {
        return "Triangle, ";
    }

    public Triangle(double side1, double side2, double side3) {
        this.side1 = side1;
        this.side2 = side2;
        this.side3 = side3;
        setPerimeter();
        setSquare();
    }

    public Triangle(double side1, double side2, double side3, String s) {
        this.side1 = side1;
        this.side2 = side2;
        this.side3 = side3;
        setPerimeter();
        setSquare();
        setColour(s);
    }

    public double getSquare() {
        return square;
    }

    public void setSquare() {
        double p = perimeter / 2;
        this.square = Math.sqrt(p * (p - side1) * (p - side2) * (p - side3));
    }

    public double getPerimeter() {
        return perimeter;
    }

    public void setPerimeter() {
        this.perimeter = side1 + side2 + side3;
    }

    public String getColour() {
        return color;
    }

    public void setColour(String s) {
        this.color = s;
    }
}
